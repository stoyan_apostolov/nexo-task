package com.nexotask.web;

import com.nexotask.NexoTaskApplication;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.stereotype.Component;
import org.springframework.web.socket.TextMessage;
import org.springframework.web.socket.WebSocketMessage;
import org.springframework.web.socket.WebSocketSession;

import java.util.Collections;
import java.util.Map;
import java.util.TreeMap;

public class BitfinexHandler extends Exchange {

    @Override
    public void afterConnectionEstablished(WebSocketSession session) throws Exception {
        session.sendMessage(new TextMessage("{ \"event\": \"subscribe\", \"channel\": \"book\"," +
                " \"symbol\": \"tBTCUSD\", \"freq\": \"F1\"}"));
    }

    @Override
    public void handleMessage(WebSocketSession session, WebSocketMessage<?> message) throws Exception {
        String payload = (String) message.getPayload();

        if (payload.matches("^\\[\\d{6}\\,\\[\\d+\\,\\d+\\,\\-?\\d+\\.\\d+\\]\\]$")) {

            Map<String, String> askMap = new TreeMap<>(Collections.reverseOrder());
            Map<String, String> bidMap = new TreeMap<>(Collections.reverseOrder());

            String price = manipulatePayload(payload)[0];
            String amount = manipulatePayload(payload)[2];
            if (amount.startsWith("-")) {
                askMap.put(price, amount);
            } else {
                bidMap.put(price, amount);
            }

            AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(NexoTaskApplication.class);
            OrderBook orderBook = (OrderBook) context.getBean("orderBook");

            if (orderBook != null) {
                if (orderBook.getAskMap() == null || orderBook.getBidMap() == null) {
                    orderBook.setAskMap(new TreeMap<>());
                    orderBook.setBidMap(new TreeMap<>());
                }
                orderBook.getAskMap().putAll(askMap);
                orderBook.getBidMap().putAll(bidMap);
            }

            printFirstTenEntries(orderBook.getAskMap());
            printFirstTenEntries(orderBook.getBidMap());
        }
    }

    // [0] - price; [1] - count; [2] - amount
    private String[] manipulatePayload(String payload) {
        String book = payload.substring(9, payload.lastIndexOf("]]"));
        return book.split(",");
    }
}
