package com.nexotask.web;

import com.nexotask.NexoTaskApplication;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.web.socket.TextMessage;
import org.springframework.web.socket.WebSocketMessage;
import org.springframework.web.socket.WebSocketSession;

import java.util.Collections;
import java.util.Map;
import java.util.TreeMap;

public class KrakenHandler extends Exchange {

    @Override
    public void afterConnectionEstablished(WebSocketSession session) throws Exception {
        session.sendMessage(new TextMessage("{\n" +
                "  \"event\": \"subscribe\",\n" +
                "  \"pair\": [\n" +
                "    \"BTC/USD\"\n" +
                "  ],\n" +
                "  \"subscription\": {\n" +
                "    \"name\": \"book\"\n" +
                "  }\n" +
                "}"));
    }

    @Override
    public void handleMessage(WebSocketSession session, WebSocketMessage<?> message) throws Exception {
        String payload = (String) message.getPayload();

        Map<String, String> askMap = new TreeMap<>(Collections.reverseOrder());
        Map<String, String> bidMap = new TreeMap<>(Collections.reverseOrder());

        if (payload.contains("\"a\"")) {
            askMap.put(manipulatePayload(payload)[0], manipulatePayload(payload)[1]);
        } else if (payload.contains("\"b\"")) {
            bidMap.put(manipulatePayload(payload)[0], manipulatePayload(payload)[1]);
        }

        AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(NexoTaskApplication.class);
        OrderBook orderBook = (OrderBook) context.getBean("orderBook");

        if (orderBook != null) {
            if (orderBook.getAskMap() == null || orderBook.getBidMap() == null) {
                orderBook.setAskMap(new TreeMap<>());
                orderBook.setBidMap(new TreeMap<>());
            }
            orderBook.getAskMap().putAll(askMap);
            orderBook.getBidMap().putAll(bidMap);
        }

        printFirstTenEntries(orderBook.getAskMap());
        printFirstTenEntries(orderBook.getBidMap());
    }

    // [0] - price; [1] - amount; [2] - timestamp
    private String[] manipulatePayload(String payload) {
        String book = payload.substring(12, payload.indexOf("]"));

        String[] split = book.split(",");
        String[] resultArr = new String[split.length];
        for (int i = 0; i < split.length; i++) {
            resultArr[i] = split[i].substring(1, split[i].lastIndexOf("\""));
        }
        return resultArr;
    }
}
