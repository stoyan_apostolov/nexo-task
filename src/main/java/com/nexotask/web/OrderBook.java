package com.nexotask.web;

import java.util.Map;

public class OrderBook {

    private Map<String, String> bidMap;
    private Map<String, String> askMap;

    public Map<String, String> getBidMap() {
        return bidMap;
    }

    public void setBidMap(Map<String, String> bidMap) {
        this.bidMap = bidMap;
    }

    public Map<String, String> getAskMap() {
        return askMap;
    }

    public void setAskMap(Map<String, String> askMap) {
        this.askMap = askMap;
    }
}
