package com.nexotask.web;

import org.springframework.web.socket.CloseStatus;
import org.springframework.web.socket.WebSocketHandler;
import org.springframework.web.socket.WebSocketSession;

import java.util.Map;

abstract class Exchange implements WebSocketHandler {

    @Override
    public void handleTransportError(WebSocketSession session, Throwable exception) throws Exception {

    }

    @Override
    public void afterConnectionClosed(WebSocketSession session, CloseStatus closeStatus) throws Exception {

    }

    @Override
    public boolean supportsPartialMessages() {
        return false;
    }

    protected void printFirstTenEntries(Map<String, String> map) {
        int count = 0;

        for (Map.Entry<String, String> entry : map.entrySet()) {
            if (count >= 10) {
                break;
            }
            System.out.println("Price: " + entry.getKey() + " Amount: " + entry.getValue());
            count++;
        }
    }
}
