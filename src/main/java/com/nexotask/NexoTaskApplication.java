package com.nexotask;

import com.nexotask.web.*;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.web.socket.client.WebSocketConnectionManager;
import org.springframework.web.socket.client.standard.StandardWebSocketClient;


import java.util.TreeMap;

@SpringBootApplication
public class NexoTaskApplication {

    private static final String BITFINEX_WEB_SOCKET_URI = "wss://api-pub.bitfinex.com/ws/2";
    private static final String KRAKEN_WEB_SOCKET_URI = "wss://ws.kraken.com";

    public static void main(String[] args) {
        SpringApplication.run(NexoTaskApplication.class, args);

        WebSocketConnectionManager bitfinexManager = new WebSocketConnectionManager(new StandardWebSocketClient(),
                new BitfinexHandler(), BITFINEX_WEB_SOCKET_URI);
        bitfinexManager.start();

        WebSocketConnectionManager krakenManager = new WebSocketConnectionManager(new StandardWebSocketClient(),
                new KrakenHandler(), KRAKEN_WEB_SOCKET_URI);
        krakenManager.start();
    }

    @Bean
    public OrderBook orderBook() {
        OrderBook orderBook = new OrderBook();
        orderBook.setAskMap(new TreeMap<>());
        orderBook.setBidMap(new TreeMap<>());
        return orderBook;
    }
}
